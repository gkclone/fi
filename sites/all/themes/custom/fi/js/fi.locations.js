(function($) {

  Drupal.behaviors.fi_locations = {
    attach: function (context, settings) {
      // Since this theme puts the location map in a tab that isn't the default,
      // we need to invoke the resize method on the map object whenever the tab
      // is switched to.
      $('.tabset__pane#two').bind('minimaAfterTabShow', function() {
        if (typeof settings.gk_locations !== 'undefined') {
          var map = settings.gk_locations.map || false;

          if (map) {
            google.maps.event.trigger(map.objMap, 'resize');
            map.objMap.setCenter(map.options.center);
          }
        }
      });
    }
  };

})(jQuery);
