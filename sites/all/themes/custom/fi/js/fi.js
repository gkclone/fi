(function($) {
  Drupal.behaviors.fi = {
    attach: function (context, settings) {
      var isPortable = Modernizr.mq('only screen and (max-width: 1039px)'),
          isLap      = Modernizr.mq('only screen and (max-width: 1039px) and (min-width: 600px)'),
          isPalm     = Modernizr.mq('only screen and (max-width: 599px');
      var header = document.getElementById('header');

      // Ensure external links open in a new window.
      $('body').on("click", "a", function() {
        var is_absolute_url = new RegExp('^http');
        var is_internal_url = new RegExp(window.location.host);
        if(is_absolute_url.test(this.href) && !is_internal_url.test(this.href)) {
          event.preventDefault();
          event.stopPropagation();
          window.open(this.href, '_blank');
        }
      });

      if (!isPortable && $('body').hasClass('front') && $('#top').css('background-position')) {
        var InitialTopBgPos = $('#top').css('background-position').split(" ");
        var InitialTopBgPosY = parseInt(InitialTopBgPos[1]);

        $(window).scroll(function(e) {
          var scrolled = $(window).scrollTop();

          var topBgPosY = InitialTopBgPosY - (scrolled * 0.05);

          if (topBgPosY > 0) {
            $('#top').css('background-position','50% ' + topBgPosY + '%');
          }
        });
      }

      if ((isLap && $('#banner').css('background-position')) || !isPortable && $('#banner').length > 0 && $('#banner').css('background-position')) {
        var InitialTopBgPos = $('#banner').css('background-position').split(" ");
        var InitialTopBgPosY = parseInt(InitialTopBgPos[1]);

        $(window).scroll(function(e) {
          var scrolled = $(window).scrollTop();

          var topBgPosY = InitialTopBgPosY - (scrolled * 0.05);

          if (topBgPosY > 0) {
            $('#banner').css('background-position','50% ' + topBgPosY + '%');
          }
        });
      }

      $(window).resize(function() {
        isPortable = Modernizr.mq('only screen and (max-width: 1039px)');
        isLap      = Modernizr.mq('only screen and (max-width: 1039px) and (min-width: 600px)');
        isPalm     = Modernizr.mq('only screen and (max-width: 599px');

        if (isPortable) {
          if (typeof window.headroom === 'undefined') {
            // construct an instance of Headroom, passing the element
            window.headroom = new Headroom(header, {
              "tolerance": 5,
              "offset": 60,
              "classes": {
                "initial": "animated",
                "pinned": "slideInDown",
                "unpinned": "slideOutUp"
              },
              onUnpin: function() {
                if (typeof window.navinate !== 'undefined' && window.navinate.expanded) {
                  window.navinate.toggleNav();
                }
              }
            });

            // initialise
            headroom.init();

            var navinateToggle = $('<div>')
              .addClass('navinate-toggle icon--reorder')
              .appendTo($(header).find('.grid'));

            window.navinate = $('.portable-nav').navinate({
              elements: [
                $('.box--menu-block-gk-core-main-menu'),
                $('.box--menu-block-gk-core-user-menu')
              ],
              toggle: navinateToggle,
              classes: {
                initial: 'is-closed',
                animateIn: "is-expanded",
                animateOut: "is-closed"
              },
              destroy: function() {
                navinateToggle.remove();
              }
            });

            $('#top, #main, #bottom, #footer, #footer-bottom, #banner').click(function() {
              if (typeof window.navinate !== 'undefined' && window.navinate.expanded) {
                window.navinate.toggleNav();
              }
            })
          }
        }
        else if (typeof headroom !== 'undefined') {
          window.headroom.destroy();
          delete window.headroom;
          window.navinate.destroy();
          delete window.navinate;
        }
      }).trigger('resize');

      // Style select form elements.
      $("select").selecter();
    }
  }
})(jQuery);
