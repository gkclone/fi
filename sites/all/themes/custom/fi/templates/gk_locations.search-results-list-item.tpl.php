<div class="location-search-results__list-item">
  <div class="location-search-results__title">
    <?php echo l($location->title, 'node/' . $location->nid) ?>
  </div>

  <?php if (!empty($location->field_location_address['und'][0]['administrative_area'])): ?>
  <div class="location-search-results__county">
    <?php echo $location->field_location_address['und'][0]['administrative_area'] ?>
  </div>
  <?php endif; ?>

  <?php if (isset($distance)): ?>
  <div class="location-search-results__distance">
    <?php echo $distance ?>
  </div>
  <?php endif; ?>

  <?php echo l('More Details', 'node/' . $location->nid, array(
    'attributes' => array(
      'class' => array('location-search-results__cta'),
    ),
  )); ?>
</div>
