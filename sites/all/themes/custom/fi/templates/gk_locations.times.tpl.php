<div class="location__times">
  <?php if ($name): ?>
    <h3 class="location__name"><?php print $name; ?></h3>
  <?php endif; ?>

  <?php if ($opening_hours): ?>
    <div class="location__opening-hours">
      <h4>Opening times</h4>
      <?php print render($opening_hours); ?>
    </div>
  <?php endif; ?>

  <?php if ($food_hours): ?>
    <div class="location__food-hours">
      <h4>Food times</h4>
      <?php print render($food_hours); ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($carvery_hours)): ?>
    <div class="location__carvery-hours">
      <h4>Carvery times</h4>
      <?php print render($carvery_hours); ?>
    </div>
  <?php endif; ?>

  <?php if ($more_details): ?>
    <?php print render($more_details); ?>
  <?php endif; ?>
</div>
