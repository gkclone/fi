<?php

/**
 * Process variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function fi_process_html(&$variables, $hook) {
  // Add less settings.
  $less_settings = less_get_settings('minima');
  drupal_add_css(drupal_get_path('theme', 'fi') . '/less/fi.less', array('less' => $less_settings));
  drupal_add_css(drupal_get_path('theme', 'fi') . '/less/ie.less', array('less' => $less_settings, 'browsers' => array('IE' => 'lt IE 9', '!IE' => FALSE)));
  $variables['styles'] = drupal_get_css();
}

/**
 * Preprocess variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function fi_preprocess_html(&$variables, $hook) {
  // Load Headroom JS plugin
  drupal_add_js(libraries_get_path('headroom') . '/dist/headroom.min.js');

  // Load Selecter jQuery plugin.
  drupal_add_css(libraries_get_path('selecter') . '/jquery.fs.selecter.css');
  drupal_add_js(libraries_get_path('selecter') . '/jquery.fs.selecter.min.js');

  // Load font
  global $is_https;
  $protocol = $is_https ? 'https' : 'http';
  drupal_add_js($protocol . '://fast.fonts.net/jsapi/d3c0a6aa-a26f-4619-b6d5-9d3f303270e5.js', 'external');
}

/**
 * Implements hook_preprocess_pane_content_header().
 */
function fi_preprocess_pane_content_header(&$variables) {
  // Disable breadcrumb.
  $variables['breadcrumb'] = '';
}

/**
 * Implements hook_preprocess_pane_header().
 */
function fi_preprocess_pane_header(&$variables) {
  $link_options = array(
    'html' => TRUE,
    'attributes' => array('title' => '← Back to home page'),
  );

  $site_name = filter_xss_admin(variable_get('site_name', 'Drupal'));

  // Branding - logo.
  $variables['branding_logo'] = '';
  $logo_path = DRUPAL_ROOT . parse_url(theme_get_setting('logo'), PHP_URL_PATH);

  if (file_exists($logo_path)) {
    $logo = theme('image', array(
      'path' => $variables['logo'],
      'alt' => $site_name . "'s logo",
      'title' => NULL,
      'width' => NULL,
      'height' => NULL,
      'attributes' => array('class' => array('branding__logo', 'portable--is-hidden')),
    ));

    $logo .= theme('image', array(
      'path' => drupal_get_path('theme', 'fi') . '/logo--portable.png',
      'alt' => $site_name . "'s logo",
      'title' => NULL,
      'width' => NULL,
      'height' => NULL,
      'attributes' => array('class' => array('branding__logo', 'desk--is-hidden')),
    ));

    $variables['branding_logo'] = l($logo, '<front>', $link_options);
  }
}

/**
 * Preprocess variables for node.tpl.php
 */
function fi_preprocess_node(&$variables) {
  if (!path_is_admin(current_path()) && $variables['type'] == 'location' && $variables['view_mode'] == 'full') {
    // Only show the 'Map & Directions' tab if we have geo information.
    if ($variables['show_map_tab'] = !empty($variables['field_location_geo'])) {
      drupal_add_js(drupal_get_path('theme', 'fi') . '/js/fi.locations.js');
    }
  }
}

/**
 * Implements hook_page_alter().
 */
function fi_page_alter(&$page) {
  $page['page_bottom']['navinate'] = array(
    '#theme_wrappers' => array('container'),
    '#attributes' => array(
      'class' => array('portable-nav'),
    ),
    '#attached' => array(
      'js' => array(
        libraries_get_path('navinate') . '/jquery.navinate.js',
      ),
    ),
  );
}

/**
 * Implements hook_minima_layout_classes_alter().
 */
function fi_minima_layout_classes_alter(&$classes, $content) {
  // Layout classes
  $classes['primary_classes'] = '';
  $classes['secondary_classes'] = ' desk--one-third';
  $classes['tertiary_classes'] = ' desk--one-third desk--pull--two-thirds lap--one-third lap--pull--two-thirds';

  if ($content['secondary'] && $content['tertiary']) {
    $classes['secondary_classes'] = ' desk--one-half lap--one-half';
    $classes['tertiary_classes'] = ' desk--one-half lap--one-half';
  }
  elseif ($content['secondary'] && !$content['tertiary']) {
    $classes['primary_classes'] = ' desk--two-thirds';
  }
  elseif (!$content['secondary'] && $content['tertiary']) {
    $classes['primary_classes'] = ' desk--two-thirds desk--push--one-third lap--two-thirds lap--push--one-third';
  }
}

/**
 * Implements hook_less_variables_alter().
 */
function fi_less_variables_alter(&$variables) {
  $variables['base-gutter-width'] = '20';
  $variables['desk-start'] = '1040px';
  $variables['lap-start'] = '600px';

  $variables['base-border-radius'] = '5px';

  $variables['colour-primary'] = '#640033';
  $variables['colour-primary-light'] = '#9c0051';
  $variables['colour-primary-lighter'] = '#cb5090';
  $variables['colour-primary-dark'] = '#3d0020';
  $variables['colour-primary-darker'] = '#14000b';

  $variables['colour-secondary'] = '#d3cbbb';
  $variables['colour-secondary-light'] = '#f3ebdc';
  $variables['colour-secondary-lighter'] = '#fffaf0';
  $variables['colour-secondary-dark'] = '#aa9f89';
  $variables['colour-secondary-darker'] = '#685e4b';

  $variables['colour-tertiary'] = '#4f6e7e';
  $variables['colour-tertiary-light'] = '#77a6bd';
  $variables['colour-tertiary-lighter'] = '#9fdefd';
  $variables['colour-tertiary-dark'] = '#365361';
  $variables['colour-tertiary-darker'] = '#1b343f';

  // $variables['colour-quaternary'] = '#';
  // $variables['colour-quaternary-light'] = '#';
  // $variables['colour-quaternary-lighter'] = '#';
  // $variables['colour-quaternary-dark'] = '#';
  // $variables['colour-quaternary-darker'] = '#';

  $variables['colour-neutral'] = '#858585';
  $variables['colour-neutral-light'] = '#c2c2c2';
  $variables['colour-neutral-lighter'] = '#d6d6d6';
  $variables['colour-neutral-dark'] = '#666666';
  $variables['colour-neutral-darker'] = '#1f1f1f';

  $variables['base-font-size'] = '15';
  $variables['base-font-colour'] = '#666666';
  $variables['base-font-family'] = '"Museo Sans W01", Museo Sans W01, Arial, arial, sans-serif';
  $variables['base-line-ratio'] = '1.7';

  $variables['alt-font-family'] = 'HandyGeorge';

  $variables['headings-font-family'] = '"Adelle W01", Adelle W01, Georgia, serif';
  $variables['headings-font-colour'] = '@colour-primary';

  // $variables['link-colour'] = '@colour-tertiary';
  // $variables['link-colour-hover'] = '@colour-primary';
  // $variables['link-colour-active'] = '@colour-primary-dark';
  // $variables['link-colour-visited'] = '@colour-tertiary-dark';
}

/**
 * Returns HTML for a fieldset form element and its children.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #attributes, #children, #collapsed, #collapsible,
 *     #description, #id, #title, #value.
 *
 * @ingroup themeable
 */
function fi_fieldset($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id'));

  $output = '<fieldset' . drupal_attributes($element['#attributes']) . '>';
  if (!empty($element['#title'])) {
    $output .= '<legend>' . $element['#title'] . '</legend>';
  }
  if (!empty($element['#description'])) {
    $output .= '<div class="fieldset__help">' . $element['#description'] . '</div>';
  }
  $output .= '<div class="fieldset-wrapper">';
  $output .= $element['#children'];
  $output .= '</div>';

  if (isset($element['#value'])) {
    $output .= $element['#value'];
  }
  $output .= "</fieldset>\n";
  return $output;
}
