<?php

// Plugin definition
$plugin = array(
  'title' => t('FI: Panels'),
  'category' => t('Site'),
  'icon' => 'fi_site_panels.png',
  'theme' => 'fi_site_panels',
  'css' => 'fi_site_panels.css',
  'regions' => array(
    'header_top' => t('Header top'),
    'header' => t('Header'),
    'navigation' => t('Navigation'),
    'banner' => t('Banner'),
    'content' => t('Content'),
    'footer' => t('Footer'),
    'footer_bottom' => t('Footer bottom'),
  ),
);
