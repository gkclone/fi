<?php
/**
 * @file
 * Template for a site panels layout.
 *
 * This template provides a basic site layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['header_top']: Content in above the header.
 *   - $content['header']: Content in the header.
 *   - $content['navigation']: Navigation content.
 *   - $content['content']: The main content.
 *   - $content['footer']: Content in the footer.
 *   - $content['footer_bottom']: Content below the footer.
 */ ?>
<div id="page">
  <?php if ($content['header_top']): ?>
  <div id="header-top" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print render($content['header_top']); ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <header id="header" role="header" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print render($content['header']); ?>
      </div>
    </div>
  </header>

  <?php if ($content['navigation']): ?>
  <nav id="navigation" role="navigation" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print render($content['navigation']); ?>
      </div>
    </div>
  </nav>
  <?php endif; ?>

  <?php if ($content['banner']): ?>
  <div id="banner" lass="container">
    <div class="container__inner">
      <div class="grid">
        <?php print render($content['banner']); ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php print render($content['content']); ?>

  <?php if ($content['footer']): ?>
  <footer id="footer" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print render($content['footer']); ?>
      </div>
    </div>
  </footer>
  <?php endif; ?>

  <?php if ($content['footer_bottom']): ?>
  <div id="footer-bottom" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print render($content['footer_bottom']); ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
</div>
