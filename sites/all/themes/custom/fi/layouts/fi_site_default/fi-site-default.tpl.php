<?php
/**
 * @file
 * Template for a site default layout.
 *
 * This template provides a basic site layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['header_top']: Content in above the header.
 *   - $content['header']: Content in the header.
 *   - $content['navigation']: Navigation content.
 *   - $content['top']: Top content.
 *   - $content['content_top']: Content above the main content.
 *   - $content['content']: The main content.
 *   - $content['content_bottom']: Content below the main content.
 *   - $content['secondary']: Secondary content.
 *   - $content['tertiary']: Tertiary content.
 *   - $content['bottom']: Bottom content.
 *   - $content['footer']: Content in the footer.
 *   - $content['footer_bottom']: Content below the footer.
 */
?>
<div id="page">
  <?php if ($content['header_top']): ?>
  <div id="header-top" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print $content['header_top']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <header id="header" role="header" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print $content['header']; ?>
      </div>
    </div>
  </header>

  <?php if ($content['navigation']): ?>
  <nav id="navigation" role="navigation" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print $content['navigation']; ?>
      </div>
    </div>
  </nav>
  <?php endif; ?>

  <?php if ($content['banner']): ?>
  <div id="banner" lass="container">
    <div class="container__inner">
      <div class="grid">
        <?php print render($content['banner']); ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if ($content['top']): ?>
  <div id="top" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print $content['top']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if ($main): ?>
  <div id="main" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php if ($primary): ?>
          <div id="primary" class="grid__cell<?php print $primary_classes; ?>">
            <div class="inner">
              <?php if ($content['content_header']): ?>
              <header id="content-header">
                <?php print $content['content_header']; ?>
              </header>
              <?php endif; ?>

              <?php if ($content['content_top']): ?>
              <div id="content-top" class="grid">
                <?php print $content['content_top']; ?>
              </div>
              <?php endif; ?>

              <?php if ($content['content']): ?>
              <div id="content" class="grid">
                <div class="grid__cell">
                  <?php print $content['content']; ?>
                </div>
              </div>
              <?php endif; ?>

              <?php if ($content['content_bottom']): ?>
              <div id="content-bottom" class="grid">
                <?php print $content['content_bottom']; ?>
              </div>
              <?php endif; ?>
            </div>
          </div>
        <?php endif; ?>

        <?php if ($content['secondary']): ?>
          <div id="secondary" class="grid__cell<?php print $secondary_classes; ?>">
            <div class="grid">
              <?php print $content['secondary']; ?>
            </div>
          </div>
        <?php endif; ?>

        <?php if ($content['tertiary']): ?>
          <div id="tertiary" class="grid__cell<?php print $tertiary_classes; ?>">
            <div class="grid">
              <?php print $content['tertiary']; ?>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if ($content['bottom']): ?>
  <div id="bottom" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print $content['bottom']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if ($content['footer']): ?>
  <footer id="footer" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print $content['footer']; ?>
      </div>
    </div>
  </footer>
  <?php endif; ?>

  <?php if ($content['footer_bottom']): ?>
  <div id="footer-bottom" class="container">
    <div class="container__inner">
      <div class="grid">
        <?php print $content['footer_bottom']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
</div>
