<?php

require_once DRUPAL_ROOT . '/' . drupal_get_path('theme', 'minima') . '/includes/minima.utilities.inc.php';

// Plugin definition
$plugin = array(
  'title' => t('FI: Default'),
  'category' => t('Site'),
  'icon' => 'fi_site_default.png',
  'theme' => 'fi_site_default',
  'css' => 'fi_site_default.css',
  'regions' => array(
    'header_top' => t('Header top'),
    'header' => t('Header'),
    'navigation' => t('Navigation'),
    'banner' => t('Banner'),
    'top' => t('Top'),
    'content_header' => t('Content header'),
    'content_top' => t('Content top'),
    'content' => t('Content'),
    'content_bottom' => t('Content bottom'),
    'secondary' => t('Secondary'),
    'tertiary' => t('Tertiary'),
    'bottom' => t('Bottom'),
    'footer' => t('Footer'),
    'footer_bottom' => t('Footer bottom'),
  ),
);

/**
 * Preprocess variables for fi-site-default.tpl.php
 */
function template_preprocess_fi_site_default(&$variables) {
  minima_get_layout_classes($variables);
}

/**
 * Process variables for fi-site-default.tpl.php
 */
function template_process_fi_site_default(&$variables, $hook) {
  // Trim whitespace from content so we can test if it exists from the template.
  foreach ($variables['content'] as $region => $content) {
    $variables['content'][$region] = trim($content);
  }

  // Is there any primary content?
  $variables['primary'] = FALSE;
  foreach(array('content_header', 'content_top', 'content', 'content_bottom') as $region) {
    if ($variables['content'][$region]) {
      $variables['primary'] = TRUE;
      break;
    }
  }

  // Is there any main content?
  $variables['main'] = FALSE;
  foreach(array('content_header', 'content_top', 'content', 'content_bottom', 'secondary', 'tertiary') as $region) {
    if ($variables['content'][$region]) {
      $variables['main'] = TRUE;
      break;
    }
  }
}
