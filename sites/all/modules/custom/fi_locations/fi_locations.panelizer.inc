<?php
/**
 * @file
 * fi_locations.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function fi_locations_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:location:fi_locations_node';
  $panelizer->title = 'FI: Location';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'location';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '516fd331-84c2-4428-8fd9-309527ca7cda';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-5f55cb71-b554-48bb-9c71-0a36c7173504';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5f55cb71-b554-48bb-9c71-0a36c7173504';
    $display->content['new-5f55cb71-b554-48bb-9c71-0a36c7173504'] = $pane;
    $display->panels['content'][0] = 'new-5f55cb71-b554-48bb-9c71-0a36c7173504';
    $pane = new stdClass();
    $pane->pid = 'new-976592b6-f985-4b95-889c-1076daa64422';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_facilities';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '976592b6-f985-4b95-889c-1076daa64422';
    $display->content['new-976592b6-f985-4b95-889c-1076daa64422'] = $pane;
    $display->panels['content'][1] = 'new-976592b6-f985-4b95-889c-1076daa64422';
    $pane = new stdClass();
    $pane->pid = 'new-8e734c44-759d-4b7f-8225-233b8cb7e651';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8e734c44-759d-4b7f-8225-233b8cb7e651';
    $display->content['new-8e734c44-759d-4b7f-8225-233b8cb7e651'] = $pane;
    $display->panels['content_header'][0] = 'new-8e734c44-759d-4b7f-8225-233b8cb7e651';
    $pane = new stdClass();
    $pane->pid = 'new-4b1b3d87-4d81-4dcd-bab2-c3aef34a63db';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_contact';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4b1b3d87-4d81-4dcd-bab2-c3aef34a63db';
    $display->content['new-4b1b3d87-4d81-4dcd-bab2-c3aef34a63db'] = $pane;
    $display->panels['secondary'][0] = 'new-4b1b3d87-4d81-4dcd-bab2-c3aef34a63db';
    $pane = new stdClass();
    $pane->pid = 'new-341c8b07-fd62-411d-a34a-a3fed41f7ede';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_times';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Pub Hours',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '341c8b07-fd62-411d-a34a-a3fed41f7ede';
    $display->content['new-341c8b07-fd62-411d-a34a-a3fed41f7ede'] = $pane;
    $display->panels['secondary'][1] = 'new-341c8b07-fd62-411d-a34a-a3fed41f7ede';
    $pane = new stdClass();
    $pane->pid = 'new-63ab0c55-cbfa-46f7-b4a1-e96c3ead4c2c';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'fi_core-fi_core_view_menu_cta';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '63ab0c55-cbfa-46f7-b4a1-e96c3ead4c2c';
    $display->content['new-63ab0c55-cbfa-46f7-b4a1-e96c3ead4c2c'] = $pane;
    $display->panels['secondary'][2] = 'new-63ab0c55-cbfa-46f7-b4a1-e96c3ead4c2c';
    $pane = new stdClass();
    $pane->pid = 'new-e09ea6c1-46dd-41db-ad26-37252ac0f549';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'fi_locations-fi_core_view_dessert_menu_cta';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'e09ea6c1-46dd-41db-ad26-37252ac0f549';
    $display->content['new-e09ea6c1-46dd-41db-ad26-37252ac0f549'] = $pane;
    $display->panels['secondary'][3] = 'new-e09ea6c1-46dd-41db-ad26-37252ac0f549';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:location:fi_locations_node'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:fi_locations_main';
  $panelizer->title = 'FI Locations: Main';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'c3ce89d5-6d14-4bf6-abf4-e61bcdf794fc';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-8f3e3a27-9d28-493b-9d5c-7e7fbf168ecf';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8f3e3a27-9d28-493b-9d5c-7e7fbf168ecf';
    $display->content['new-8f3e3a27-9d28-493b-9d5c-7e7fbf168ecf'] = $pane;
    $display->panels['content'][0] = 'new-8f3e3a27-9d28-493b-9d5c-7e7fbf168ecf';
    $pane = new stdClass();
    $pane->pid = 'new-e29ad3ad-e943-4509-8644-0514293b5f30';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'e29ad3ad-e943-4509-8644-0514293b5f30';
    $display->content['new-e29ad3ad-e943-4509-8644-0514293b5f30'] = $pane;
    $display->panels['content'][1] = 'new-e29ad3ad-e943-4509-8644-0514293b5f30';
    $pane = new stdClass();
    $pane->pid = 'new-2096a331-0b0f-45f6-9e62-b6430d3224f4';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2096a331-0b0f-45f6-9e62-b6430d3224f4';
    $display->content['new-2096a331-0b0f-45f6-9e62-b6430d3224f4'] = $pane;
    $display->panels['content_header'][0] = 'new-2096a331-0b0f-45f6-9e62-b6430d3224f4';
    $pane = new stdClass();
    $pane->pid = 'new-201bde37-c3a6-4364-baa7-8fee082e957a';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'fi_locations-fi_locations_short_search_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Refine Search',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '201bde37-c3a6-4364-baa7-8fee082e957a';
    $display->content['new-201bde37-c3a6-4364-baa7-8fee082e957a'] = $pane;
    $display->panels['secondary'][0] = 'new-201bde37-c3a6-4364-baa7-8fee082e957a';
    $pane = new stdClass();
    $pane->pid = 'new-1e73dbfb-a384-4359-bfa3-ee1e4fd71bc1';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-secondary-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '1e73dbfb-a384-4359-bfa3-ee1e4fd71bc1';
    $display->content['new-1e73dbfb-a384-4359-bfa3-ee1e4fd71bc1'] = $pane;
    $display->panels['secondary'][1] = 'new-1e73dbfb-a384-4359-bfa3-ee1e4fd71bc1';
    $pane = new stdClass();
    $pane->pid = 'new-04d2ae7f-a538-430d-a26b-b112b0c13def';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '04d2ae7f-a538-430d-a26b-b112b0c13def';
    $display->content['new-04d2ae7f-a538-430d-a26b-b112b0c13def'] = $pane;
    $display->panels['secondary'][2] = 'new-04d2ae7f-a538-430d-a26b-b112b0c13def';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:fi_locations_main'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:fi_locations_search';
  $panelizer->title = 'FI Locations: Search';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'fd505a02-3e1f-4f66-93b8-a8e54707df42';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-79e39c2f-cd5a-404b-a381-11feec25f236';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_search_results_map';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '79e39c2f-cd5a-404b-a381-11feec25f236';
    $display->content['new-79e39c2f-cd5a-404b-a381-11feec25f236'] = $pane;
    $display->panels['content'][0] = 'new-79e39c2f-cd5a-404b-a381-11feec25f236';
    $pane = new stdClass();
    $pane->pid = 'new-f01d8000-9d71-43af-950a-b0a133ea0a40';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_search_results_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f01d8000-9d71-43af-950a-b0a133ea0a40';
    $display->content['new-f01d8000-9d71-43af-950a-b0a133ea0a40'] = $pane;
    $display->panels['content'][1] = 'new-f01d8000-9d71-43af-950a-b0a133ea0a40';
    $pane = new stdClass();
    $pane->pid = 'new-752ccb56-669e-4976-accd-e837c081afcd';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '752ccb56-669e-4976-accd-e837c081afcd';
    $display->content['new-752ccb56-669e-4976-accd-e837c081afcd'] = $pane;
    $display->panels['content'][2] = 'new-752ccb56-669e-4976-accd-e837c081afcd';
    $pane = new stdClass();
    $pane->pid = 'new-40ecf441-7aef-4614-aa45-26ed8e2f800f';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '40ecf441-7aef-4614-aa45-26ed8e2f800f';
    $display->content['new-40ecf441-7aef-4614-aa45-26ed8e2f800f'] = $pane;
    $display->panels['content_header'][0] = 'new-40ecf441-7aef-4614-aa45-26ed8e2f800f';
    $pane = new stdClass();
    $pane->pid = 'new-a0ba5306-e373-43b7-b4ef-21dcae0e2ebc';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-secondary-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a0ba5306-e373-43b7-b4ef-21dcae0e2ebc';
    $display->content['new-a0ba5306-e373-43b7-b4ef-21dcae0e2ebc'] = $pane;
    $display->panels['secondary'][0] = 'new-a0ba5306-e373-43b7-b4ef-21dcae0e2ebc';
    $pane = new stdClass();
    $pane->pid = 'new-7093415f-7625-4a61-9637-31c30d4a0e50';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_search_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7093415f-7625-4a61-9637-31c30d4a0e50';
    $display->content['new-7093415f-7625-4a61-9637-31c30d4a0e50'] = $pane;
    $display->panels['top'][0] = 'new-7093415f-7625-4a61-9637-31c30d4a0e50';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:fi_locations_search'] = $panelizer;

  return $export;
}
