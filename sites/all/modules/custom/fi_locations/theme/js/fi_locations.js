(function($) {

Drupal.behaviors.fi_locations = {
  attach: function (context, settings) {
    $('#gk-locations-search-form').on(
      'click',
      '#edit-search .form-item__toggle, #edit-advanced-search .form-item__toggle',
      function(e) {
        $('#edit-advanced-search').fadeToggle('fast');

        e.preventDefault();
        return false;
      }
    );

    $('#edit-advanced-search').append('<a href="#" class="form-item__toggle"></a>');

    $('.box--gk-locations-gk-locations-search-results-list > .box__inner > .box__title').click(function() {
      if (Modernizr.mq('only screen and (min-width: 1024px)')) {
        $('.location-search-results').toggle('fast');
        $(this).toggleClass('content-is-hidden');
      }
    });

    $('<a href="#" class="location-search-results__toggle icon--angle-left"></a>')
      .click(function(e) {
        $(this)
          .toggleClass('icon--angle-left icon--angle-right')
          .parent()
          .toggleClass('is-closed');

        $('.box--gk-locations-gk-locations-search-results-map .box__content')
          .toggleClass('is-expanded');

        window.setTimeout(function() {
          google.maps.event.trigger(Drupal.settings.gk_locations.map.objMap, 'resize')
        }, 200);

        e.preventDefault;
        return false;
      })
      .appendTo('.location-search-results');
  }
}

})(jQuery);
