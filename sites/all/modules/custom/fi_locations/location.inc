<?php

/**
 *
 */
abstract class LocationMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->team = array(
      new MigrateTeamMember('Brendan Hurley', 'brendanhurley@greeneking.com',
                            t('Web Developer')),
    );

    //$this->issuePattern = 'http://drupal.org/node/:id:';
  }
}

/**
 *
 */
class LocationNodeMigration extends LocationMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description =
      t('Migrate locations from a CSV to nodes');

    // Define CSV columns.
    $columns = array(
      array('id', 'ID'),
      array('title', 'Name'),
      array('house_id', 'House ID'),
      array('address_1', 'Address 1'),
      array('address_2', 'Address 2'),
      array('town', 'Town'),
      array('county', 'County'),
      array('postcode', 'Postcode'),
      array('telephone', 'Telephone'),
      array('email', 'Email'),
      array('general_manager', 'General manager'),
      array('mon_start', 'Mon Open'),
      array('mon_end', 'Mon Close'),
      array('tues_start', 'Tues Open'),
      array('tues_end', 'Tues Close'),
      array('weds_start', 'Weds Open'),
      array('weds_end', 'Weds Close'),
      array('thurs_start', 'Thurs Open'),
      array('thurs_end', 'Thurs Close'),
      array('fri_start', 'Fri Open'),
      array('fri_end', 'Fri Close'),
      array('sat_start', 'Sat Open'),
      array('sat_end', 'Sat Close'),
      array('sun_start', 'Sun Open'),
      array('sun_end', 'Sun Close'),
      array('food_mon_start', 'Food Mon Open'),
      array('food_mon_end', 'Food Mon Close'),
      array('food_tues_start', 'Food Tues Open'),
      array('food_tues_end', 'Food Tues Close'),
      array('food_weds_start', 'Food Weds Open'),
      array('food_weds_end', 'Food Weds Close'),
      array('food_thurs_start', 'Food Thurs Open'),
      array('food_thurs_end', 'Food Thurs Close'),
      array('food_fri_start', 'Food Fri Open'),
      array('food_fri_end', 'Food Fri Close'),
      array('food_sat_start', 'Food Sat Open'),
      array('food_sat_end', 'Food Sat Close'),
      array('food_sun_start', 'Food Sun Open'),
      array('food_sun_end', 'Food Sun Close'),
      array('carvery_mon_start', 'Carvery Mon Open'),
      array('carvery_mon_end', 'Carvery Mon Close'),
      array('carvery_tues_start', 'Carvery Tues Open'),
      array('carvery_tues_end', 'Carvery Tues Close'),
      array('carvery_weds_start', 'Carvery Weds Open'),
      array('carvery_weds_end', 'Carvery Weds Close'),
      array('carvery_thurs_start', 'Carvery Thurs Open'),
      array('carvery_thurs_end', 'Carvery Thurs Close'),
      array('carvery_fri_start', 'Carvery Fri Open'),
      array('carvery_fri_end', 'Carvery Fri Close'),
      array('carvery_sat_start', 'Carvery Sat Open'),
      array('carvery_sat_end', 'Carvery Sat Close'),
      array('carvery_sun_start', 'Carvery Sun Open'),
      array('carvery_sun_end', 'Carvery Sun Close'),
      array('carvery2_mon_start', 'Carvery2 Mon Open'),
      array('carvery2_mon_end', 'Carvery2 Mon Close'),
      array('carvery2_tues_start', 'Carvery2 Tues Open'),
      array('carvery2_tues_end', 'Carvery2 Tues Close'),
      array('carvery2_weds_start', 'Carvery2 Weds Open'),
      array('carvery2_weds_end', 'Carvery2 Weds Close'),
      array('carvery2_thurs_start', 'Carvery2 Thurs Open'),
      array('carvery2_thurs_end', 'Carvery2 Thurs Close'),
      array('carvery2_fri_start', 'Carvery2 Fri Open'),
      array('carvery2_fri_end', 'Carvery2 Fri Close'),
      array('carvery2_sat_start', 'Carvery2 Sat Open'),
      array('carvery2_sat_end', 'Carvery2 Sat Close'),
      array('carvery2_sun_start', 'Carvery2 Sun Open'),
      array('carvery2_sun_end', 'Carvery2 Sun Close'),
      array('golden_years_menu', 'Golden Years Menu'),
      array('parking', 'Parking'),
      array('free_wifi', 'Free Wi-Fi'),
      array('chilrens_area', "Indoor Children's Play Area"),
      array('beer_garden', 'Beer garden'),
      array('baby_changing', 'Baby Changing'),
      array('disabled_access', 'Disabled Facilities & Wheelchair Access'),
      array('salad_bar', 'Salad Bar'),
      array('ice_cream', 'Ice Cream'),
      array('cakeaway', 'Cakeaway'),
      array('facebook', 'Facebook'),
      array('twitter', 'Twitter'),
      array('quiz_night', 'Quiz Night'),
      array('entertainment', 'Entertainment'),
      array('other', 'Other'),
      array('images', 'Images'),
      array('supporting_copy', 'Supporting copy'),
      array('notes', 'Notes'),
    );
    $path = drupal_get_path('module', 'fi_locations') . '/csv/locations.csv';

    // Define CSV options.
    $options = array(
      'header_rows' => 1,
      'embedded_newlines' => TRUE,
    );

    // Define extra fields.
    $fields = array(
      'facilities' => 'Facilities',
    );

    // Define source and destination.
    $this->source = new MigrateSourceCSV($path, $columns, $options, $fields);
    $this->destination = new MigrateDestinationNode('location');

    // Define map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'house_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'House ID',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Add field mappings.
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('status')
         ->defaultValue(NODE_PUBLISHED);

    $this->addFieldMapping('body', 'supporting_copy');
    $this->addFieldMapping('body:format')
         ->defaultValue('full_html');

    $this->addFieldMapping('field_location_facilities', 'facilities');
    $this->addFieldMapping('field_location_facilities:create_term')
         ->defaultValue(TRUE);

    $this->addFieldMapping('field_location_house_id', 'house_id');
    $this->addFieldMapping('field_location_telephone', 'telephone');
    $this->addFieldMapping('field_location_email', 'email');
    $this->addFieldMapping('field_location_social_facebook', 'facebook');
    $this->addFieldMapping('field_location_social_twitter', 'twitter');

    // Unmigrated source fields.
    $this->addUnmigratedSources(array(
      'id',
      'title',
      'house_id',
      'address_1',
      'address_2',
      'town',
      'county',
      'postcode',
      'telephone',
      'email',
      'general_manager',
      'mon_start',
      'mon_end',
      'tues_start',
      'tues_end',
      'weds_start',
      'weds_end',
      'thurs_start',
      'thurs_end',
      'fri_start',
      'fri_end',
      'sat_start',
      'sat_end',
      'sun_start',
      'sun_end',
      'food_mon_start',
      'food_mon_end',
      'food_tues_start',
      'food_tues_end',
      'food_weds_start',
      'food_weds_end',
      'food_thurs_start',
      'food_thurs_end',
      'food_fri_start',
      'food_fri_end',
      'food_sat_start',
      'food_sat_end',
      'food_sun_start',
      'food_sun_end',
      'carvery_mon_start',
      'carvery_mon_end',
      'carvery_tues_start',
      'carvery_tues_end',
      'carvery_weds_start',
      'carvery_weds_end',
      'carvery_thurs_start',
      'carvery_thurs_end',
      'carvery_fri_start',
      'carvery_fri_end',
      'carvery_sat_start',
      'carvery_sat_end',
      'carvery_sun_start',
      'carvery_sun_end',
      'carvery2_mon_start',
      'carvery2_mon_end',
      'carvery2_tues_start',
      'carvery2_tues_end',
      'carvery2_weds_start',
      'carvery2_weds_end',
      'carvery2_thurs_start',
      'carvery2_thurs_end',
      'carvery2_fri_start',
      'carvery2_fri_end',
      'carvery2_sat_start',
      'carvery2_sat_end',
      'carvery2_sun_start',
      'carvery2_sun_end',
      'golden_years_menu',
      'parking',
      'free_wifi',
      'chilrens_area',
      'beer_garden',
      'baby_changing',
      'disabled_access',
      'salad_bar',
      'ice_cream',
      'cakeaway',
      'facebook',
      'twitter',
      'quiz_night',
      'entertainment',
      'other',
      'images',
      'supporting_copy',
      'notes',
    ));

    // Unmigrated destination fields.
    $this->addUnmigratedDestinations(array(
      'uid',
      'created',
      'changed',
      'promote',
      'sticky',
      'revision',
      'log',
      'language',
      'tnid',
      'translate',
      'revision_uid',
      'is_new',
      'body:summary',
      'body:language',
      'field_location_address',
      'field_location_address:administrative_area',
      'field_location_address:sub_administrative_area',
      'field_location_address:locality',
      'field_location_address:dependent_locality',
      'field_location_address:postal_code',
      'field_location_address:thoroughfare',
      'field_location_address:premise',
      'field_location_address:sub_premise',
      'field_location_address:organisation_name',
      'field_location_address:name_line',
      'field_location_address:first_name',
      'field_location_address:last_name',
      'field_location_address:data',
      'field_location_facilities:source_type',
      'field_location_facilities:ignore_case',
      'field_location_fax',
      'field_location_fax:language',
      'field_location_geo',
      'field_location_geo:geo_type',
      'field_location_geo:lat',
      'field_location_geo:lon',
      'field_location_geo:left',
      'field_location_geo:top',
      'field_location_geo:right',
      'field_location_geo:bottom',
      'field_location_geo:geohash',
      'field_location_images',
      'field_location_images:file_class',
      'field_location_images:language',
      'field_location_images:preserve_files',
      'field_location_images:destination_dir',
      'field_location_images:destination_file',
      'field_location_images:file_replace',
      'field_location_images:source_dir',
      'field_location_images:urlencode',
      'field_location_images:alt',
      'field_location_images:title',
      'field_location_price_band',
      'field_location_price_band:source_type',
      'field_location_price_band:create_term',
      'field_location_price_band:ignore_case',
      'field_location_opening_hours',
      'field_location_opening_hours:starthours',
      'field_location_opening_hours:endhours',
      'field_location_food_hours',
      'field_location_food_hours:starthours',
      'field_location_food_hours:endhours',
      'field_location_carvery_hours',
      'field_location_carvery_hours:starthours',
      'field_location_carvery_hours:endhours',
      'field_location_telephone:language',
      'field_location_social_facebook:language',
      'field_location_social_twitter:language',
      'path',
      'comment',
    ));
  }

  /**
   *
   */
  public function prepareRow($row) {

    // Facilities
    $row->facilities = array();

    if ($row->free_wifi) {
      $row->facilities[] = 'Free Wi-Fi';
    }
    if ($row->chilrens_area) {
      $row->facilities[] = "Indoor Children's Play Area";
    }
    if ($row->beer_garden ) {
      $row->facilities[] = 'Beer garden';
    }
    if ($row->disabled_access ) {
      $row->facilities[] = 'Disabled Facilities & Wheelchair Access';
    }
    if ($row->parking) {
      $row->facilities[] = 'Parking';
    }
    if ($row->baby_changing) {
      $row->facilities[] = 'Baby Changing';
    }
    if ($row->salad_bar) {
      $row->facilities[] = 'Salad bar';
    }
    if ($row->ice_cream) {
      $row->facilities[] = 'Ice cream';
    }
    if ($row->cakeaway) {
      $row->facilities[] = 'Cakeaway';
    }
  }

  /**
   *
   */
  public function prepare($node, stdClass $row) {
    // Address.
    $node->field_location_address[LANGUAGE_NONE][] = array(
      'country' => 'GB',
      'thoroughfare' => $row->address_1,
      'premise' => $row->address_2,
      'locality' => $row->town,
      'administrative_area' => $row->county,
      'postal_code' => $row->postcode,
    );

    // Opening hours.
    foreach(array('sun', 'mon', 'tues', 'weds', 'thurs', 'fri', 'sat') as $index => $day) {
      $start = $day . '_start';
      $end = $day . '_end';

      if (is_numeric($row->$start) && is_numeric($row->$end)) {
        $node->field_location_opening_hours[LANGUAGE_NONE][] = array(
          'day' => $index,
          'starthours' => $row->$start,
          'endhours' => $row->$end,
        );
      }
    }

    // Food hours.
    foreach(array('sun', 'mon', 'tues', 'weds', 'thurs', 'fri', 'sat') as $index => $day) {
      $start = 'food_' . $day . '_start';
      $end = 'food_' . $day . '_end';

      if (is_numeric($row->$start) && is_numeric($row->$end)) {
        $node->field_location_food_hours[LANGUAGE_NONE][] = array(
          'day' => $index,
          'starthours' => $row->$start,
          'endhours' => $row->$end,
        );
      }
    }

    // Food hours.
    foreach(array('sun', 'mon', 'tues', 'weds', 'thurs', 'fri', 'sat') as $index => $day) {
      $start = 'carvery_' . $day . '_start';
      $end = 'carvery_' . $day . '_end';

      if (is_numeric($row->$start) && is_numeric($row->$end)) {
        $node->field_location_carvery_hours[LANGUAGE_NONE][] = array(
          'day' => $index,
          'starthours' => $row->$start,
          'endhours' => $row->$end,
        );
      }

      $start = 'carvery2_' . $day . '_start';
      $end = 'carvery2_' . $day . '_end';

      if (is_numeric($row->$start) && is_numeric($row->$end)) {
        $node->field_location_carvery_hours[LANGUAGE_NONE][] = array(
          'day' => $index,
          'starthours' => $row->$start,
          'endhours' => $row->$end,
        );
      }
    }
  }
}
