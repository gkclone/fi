<?php

/**
 * Implementation of hook_filter_info()
 */
function fi_promotions_filter_info() {
  $filters['promotion_embed'] = array(
    'title' => t('Insert promotion/promotion group'),
    'description' => t('Embed promotions using the syntax [[promotion:pid]] or [[promotion_group:pgid]]'),
    'prepare callback' => 'fi_promotions_filter_promotion_embed_prepare',
    'process callback' => 'fi_promotions_filter_promotion_embed_process',
    'tips callback'  => 'fi_promotions_filter_promotion_embed_tips',
    'cache' => FALSE,
  );

  return $filters;
}

/**
 * Prepare callback for hook_filter
 */
function fi_promotions_filter_promotion_embed_prepare($text, $filter, $format, $langcode, $cache, $cache_id) {
  return $text;
}

/**
 * Process callback for hook_filter
 */
function fi_promotions_filter_promotion_embed_process($text, $filter, $format, $langcode, $cache, $cache_id) {
  return preg_replace_callback('/\[\[(promotion(_group)?):(\d+)\]\]/', '_promotion_make_replacements', $text);
}

/**
 * Tips callback for hook_filter
 */
function fi_promotions_filter_promotion_embed_tips($filter, $format, $long) {
  return t('[[promotion:123]] or [[promotion_group:123]] - Insert a promotion or promotion group');
}

/**
 * Provides the replacement html to be rendered in place of the embed code.
 *
 * @param $matches
 *    numeric node id that has been captured by preg_replace_callback
 * @return
 *    The rendered HTML replacing the embed code
 */
function _promotion_make_replacements($matches) {
  switch ($matches[1]) {
    case 'promotion':
      if ($promotion = promotion_load($matches[3])) {
        return render(promotion_view($promotion));
      }
    break;

    case 'promotion_group':
      if ($promotion_group = promotion_group_load($matches[3])) {
        return render(promotion_group_view($promotion_group));
      }
    break;
  }
}

// /**
//  * Implements hook_theme_registry_alter()
//  * This is where we add our default template for the fckeditor view page template.
//  */
// function fi_promotions_theme_registry_alter(&$theme_registry) {
//   //Add 'html--ckeditor-node-embed.tpl.php' template file
//   $theme_registry['html__ckeditor_promotion_embed'] = Array();
//   $theme_registry['html__ckeditor_promotion_embed']['template'] = 'html--ckeditor-node-embed';
//   $theme_registry['html__ckeditor_promotion_embed']['path'] = drupal_get_path('module', 'promotion_embed') . "/theme";
//   $theme_registry['html__ckeditor_promotion_embed']['render element'] = 'page';
//   $theme_registry['html__ckeditor_promotion_embed']['base hook'] = 'html';
//   $theme_registry['html__ckeditor_promotion_embed']['type'] = 'theme_engine';
//   $theme_registry['html__ckeditor_promotion_embed']['theme path'] = path_to_theme();
//   $theme_registry['html__ckeditor_promotion_embed']['preprocess functions'] = Array();
//   $theme_registry['html__ckeditor_promotion_embed']['process functions'] = Array();

//   //Add 'page--ckeditor-node-embed.tpl.php' template file
//   $theme_registry['page__ckeditor_promotion_embed'] = Array();
//   $theme_registry['page__ckeditor_promotion_embed']['template'] = 'page--ckeditor-node-embed';
//   $theme_registry['page__ckeditor_promotion_embed']['path'] = drupal_get_path('module', 'promotion_embed') . "/theme";
//   $theme_registry['page__ckeditor_promotion_embed']['render element'] = 'page';
//   $theme_registry['page__ckeditor_promotion_embed']['base hook'] = 'page';
//   $theme_registry['page__ckeditor_promotion_embed']['type'] = 'theme_engine';
//   $theme_registry['page__ckeditor_promotion_embed']['theme path'] = path_to_theme();
//   $theme_registry['page__ckeditor_promotion_embed']['preprocess functions'] = Array();
//   $theme_registry['page__ckeditor_promotion_embed']['process functions'] = Array();
// }

// /**
//  * Make compatible with views 2 for default view.
//  */
// function fi_promotions_views_api() {
//   return array('api' => 3);
// }

// /**
//  * Implementation of hook_views_pre_render() {
//  */
// function fi_promotions_views_pre_render(&$view) {
//   if ($view->name == 'ckeditor_promotion_embed' && $view->current_display == 'page_1') {
//     fi_promotions_suppress_admin_menu();
//   }
// }

// /**
//  * Implementation of hook_views_default_views().
//  */
// function fi_promotions_views_default_views() {
//   $views = array();

//   if (module_exists('ckeditor') || module_exists('wysiwyg')) {
//     $pathCK = drupal_get_path('module', 'fi_promotions') . '/ckeditor/ckeditor_promotion_embed.view.inc';
//     include_once($pathCK);
//     $views[$view->name] = $view;
//   }

//   return $views;
// }

// /**
//  * Implements hook_views_data_alter()
//  */
// function fi_promotions_views_data_alter(&$data)  {
//   $data['views']['promotion_embed'] = array(
//     'title' => t('Node embed add area'),
//     'help' => t('Provide links to add nodes.'),
//     'area' => array(
//       'handler' => 'views_handler_promotion_embed_add_area',
//     ),
//   );
// }

// /**
//  * Implement hook_form_alter()
//  * add a validation handler to nodes with promotion_embed.
//  */
// function fi_promotions_form_alter(&$form, &$form_state, $form_id) {
//   $form['#validate'][] = 'promotion_embed_validate';
// }

// /**
//  * validation for the promotion_embed filter.
//  * we do not allow nodes to embed in themselves.
//  * results in segment fault.
//  */
// function fi_promotions_validate($node, $form) {
//   if (isset($form['values']['nid'])) {

//     $nid = $form['values']['nid'];

//     $needle = "[[nid:{$nid}]]";

//     $num = 0;
//     $language = (isset($form['values']['language']) && $form['values']['language']) ? $form['values']['language'] : LANGUAGE_NONE;

//     while (isset($form['values']['body'][$language][$num])) {
//       $found = strpos($form['values']['body'][$form['values']['language']][$num]['value'], $needle);

//       if ($found == TRUE) {
//         form_set_error('edit-body', t('A node is not allowed to embed in itself.'));
//       }

//       $num++;
//     }
//   }
// }

/**
 * Implementing the Wysiwyg API
 * Register a directory containing Wysiwyg plugins.
 *
 * @param $type
 *   The type of objects being collected: either 'plugins' or 'editors'.
 * @return
 *   A sub-directory of the implementing module that contains the corresponding
 *   plugin files. This directory must only contain integration files for
 *   Wysiwyg module.
 */
function fi_promotions_wysiwyg_include_directory($type) {
  switch ($type) {
    case 'plugins':
      // You can just return $type, if you place your Wysiwyg plugins into a
      // sub-directory named 'plugins'.
      return $type;
  }
}

// /**
//  *  Implementation of hook_init - attach the needed css files if we're on a form page
//  */
// function fi_promotions_init() {
//   drupal_add_css(drupal_get_path('module', 'fi_promotions') . '/plugins/fi_promotions/fi_promotions.css');
// }

// /**
//  * Implements hook_entity_info_alter().
//  */
// function fi_promotions_entity_info_alter(&$entity_info) {
//   if (isset($entity_info['node'])) {
//     $entity_info['node']['view modes'] += array(
//       'promotion_embed' => array(
//         'label' => 'Node embed',
//         'custom settings' => FALSE,
//       ),
//     );
//   }
// }

// /**
//  * Implements template_preproccess_node().
//  */
// function fi_promotions_preprocess_node(&$variables) {
//   if ($variables['view_mode'] == 'promotion_embed') {
//     $node = $variables['node'];
//     $variables['theme_hook_suggestions'][] = 'node__promotion_embed';
//     $variables['theme_hook_suggestions'][] = 'node__' . $node->type . '__promotion_embed';
//   }
// }

// /**
//  * Suppress Admin Menu if it is present
//  */
// function promotion_embed_suppress_admin_menu() {
//   if (module_exists('admin_menu')) {
//     admin_menu_suppress();
//   }
// }

/**
 * Implements hook_filter_default_formats_alter().
 */
function fi_promotions_filter_default_formats_alter(&$formats) {
  if (isset($formats['full_html']['filters'])) {
    $formats['full_html']['filters']['promotion_embed'] = array(
      'weight' => 1,
      'status' => 1,
      'settings' => array(),
    );
  }
}
