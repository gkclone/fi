<?php
/**
 * @file
 * fi_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function fi_core_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_panel_fi_default';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -100;
  $handler->conf = array(
    'title' => 'FI: Default',
    'no_blocks' => 1,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(),
      'logic' => 'and',
    ),
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
  );
  $display = new panels_display();
  $display->layout = 'fi_site_default';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => FALSE,
        'fixed_width' => '',
        'column_separation' => '0.5em',
        'region_separation' => '0.5em',
        'row_separation' => '0.5em',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'content',
          1 => 'sidebar',
        ),
        'parent' => 'main',
        'class' => 'main-content',
      ),
      'content' => array(
        'type' => 'region',
        'title' => 'Content',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'sidebar' => array(
        'type' => 'region',
        'title' => 'Sidebar',
        'width' => '200',
        'width_type' => 'px',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'header',
        ),
        'parent' => 'main',
        'class' => 'header',
      ),
      'header' => array(
        'type' => 'region',
        'title' => 'Header',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
      'header' => NULL,
      'header_top' => NULL,
      'navigation' => NULL,
      'footer' => NULL,
      'footer_bottom' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'd94877a5-3bfb-43ec-bb39-8334a32cf9bc';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-7c2303c2-1cc9-4661-b5e4-47cbf98b53d9';
    $pane->panel = 'banner';
    $pane->type = 'block';
    $pane->subtype = 'fi_core-fi_core_page_strapline';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7c2303c2-1cc9-4661-b5e4-47cbf98b53d9';
    $display->content['new-7c2303c2-1cc9-4661-b5e4-47cbf98b53d9'] = $pane;
    $display->panels['banner'][0] = 'new-7c2303c2-1cc9-4661-b5e4-47cbf98b53d9';
    $pane = new stdClass();
    $pane->pid = 'new-cf9da8d8-8892-4ac1-a516-a7dc71c8d021';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cf9da8d8-8892-4ac1-a516-a7dc71c8d021';
    $display->content['new-cf9da8d8-8892-4ac1-a516-a7dc71c8d021'] = $pane;
    $display->panels['content'][0] = 'new-cf9da8d8-8892-4ac1-a516-a7dc71c8d021';
    $pane = new stdClass();
    $pane->pid = 'new-aea6a2d8-9e43-4335-aea6-032dd11460a0';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aea6a2d8-9e43-4335-aea6-032dd11460a0';
    $display->content['new-aea6a2d8-9e43-4335-aea6-032dd11460a0'] = $pane;
    $display->panels['content_header'][0] = 'new-aea6a2d8-9e43-4335-aea6-032dd11460a0';
    $pane = new stdClass();
    $pane->pid = 'new-7d8485e6-d920-4158-898d-b52ae0615b5c';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'fi_core-fi_core_footer_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--three-tenths lap--one-third palm--one-half',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7d8485e6-d920-4158-898d-b52ae0615b5c';
    $display->content['new-7d8485e6-d920-4158-898d-b52ae0615b5c'] = $pane;
    $display->panels['footer'][0] = 'new-7d8485e6-d920-4158-898d-b52ae0615b5c';
    $pane = new stdClass();
    $pane->pid = 'new-8c23a44b-be9d-4ae5-89bd-b862a5dcc719';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-helpful-links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--three-tenths lap--one-third palm--one-half',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '8c23a44b-be9d-4ae5-89bd-b862a5dcc719';
    $display->content['new-8c23a44b-be9d-4ae5-89bd-b862a5dcc719'] = $pane;
    $display->panels['footer'][1] = 'new-8c23a44b-be9d-4ae5-89bd-b862a5dcc719';
    $pane = new stdClass();
    $pane->pid = 'new-64ff7697-8b6f-4cd4-9bea-93985b1f73ce';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'fi_core-fi_footer_affiliates';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--four-tenths lap--one-third',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '64ff7697-8b6f-4cd4-9bea-93985b1f73ce';
    $display->content['new-64ff7697-8b6f-4cd4-9bea-93985b1f73ce'] = $pane;
    $display->panels['footer'][2] = 'new-64ff7697-8b6f-4cd4-9bea-93985b1f73ce';
    $pane = new stdClass();
    $pane->pid = 'new-8b4e502c-8f09-4cd3-a071-bed58e01c504';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_core-copyright';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8b4e502c-8f09-4cd3-a071-bed58e01c504';
    $display->content['new-8b4e502c-8f09-4cd3-a071-bed58e01c504'] = $pane;
    $display->panels['footer_bottom'][0] = 'new-8b4e502c-8f09-4cd3-a071-bed58e01c504';
    $pane = new stdClass();
    $pane->pid = 'new-e0225c78-1fb7-4bf6-b0b0-2314394e28cd';
    $pane->panel = 'header';
    $pane->type = 'pane_header';
    $pane->subtype = 'pane_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = 'e0225c78-1fb7-4bf6-b0b0-2314394e28cd';
    $display->content['new-e0225c78-1fb7-4bf6-b0b0-2314394e28cd'] = $pane;
    $display->panels['header'][0] = 'new-e0225c78-1fb7-4bf6-b0b0-2314394e28cd';
    $pane = new stdClass();
    $pane->pid = 'new-8e2e7152-6070-4bf8-9cd6-764b30865919';
    $pane->panel = 'header_top';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8e2e7152-6070-4bf8-9cd6-764b30865919';
    $display->content['new-8e2e7152-6070-4bf8-9cd6-764b30865919'] = $pane;
    $display->panels['header_top'][0] = 'new-8e2e7152-6070-4bf8-9cd6-764b30865919';
    $pane = new stdClass();
    $pane->pid = 'new-5af73ebc-c358-4731-a418-2ac7e6a83a21';
    $pane->panel = 'navigation';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5af73ebc-c358-4731-a418-2ac7e6a83a21';
    $display->content['new-5af73ebc-c358-4731-a418-2ac7e6a83a21'] = $pane;
    $display->panels['navigation'][0] = 'new-5af73ebc-c358-4731-a418-2ac7e6a83a21';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['site_template_panel_fi_default'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_panel_fi_panels';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -101;
  $handler->conf = array(
    'title' => 'FI: Panels',
    'no_blocks' => 1,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_is_panelized',
          'settings' => NULL,
          'context' => array(
            0 => 'node',
            1 => 'account',
            2 => 'term',
          ),
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
  );
  $display = new panels_display();
  $display->layout = 'fi_site_panels';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => FALSE,
        'fixed_width' => '',
        'column_separation' => '0.5em',
        'region_separation' => '0.5em',
        'row_separation' => '0.5em',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'content',
          1 => 'sidebar',
        ),
        'parent' => 'main',
        'class' => 'main-content',
      ),
      'content' => array(
        'type' => 'region',
        'title' => 'Content',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'sidebar' => array(
        'type' => 'region',
        'title' => 'Sidebar',
        'width' => '200',
        'width_type' => 'px',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'header',
        ),
        'parent' => 'main',
        'class' => 'header',
      ),
      'header' => array(
        'type' => 'region',
        'title' => 'Header',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
      'header' => NULL,
      'header_top' => NULL,
      'navigation' => NULL,
      'footer' => NULL,
      'footer_bottom' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'banner' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'f8b09475-8a5e-4364-996d-c0aea61020b5';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-224eef88-2b8f-4723-9db2-2370b7e0f2e2';
    $pane->panel = 'banner';
    $pane->type = 'block';
    $pane->subtype = 'fi_core-fi_core_page_strapline';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '224eef88-2b8f-4723-9db2-2370b7e0f2e2';
    $display->content['new-224eef88-2b8f-4723-9db2-2370b7e0f2e2'] = $pane;
    $display->panels['banner'][0] = 'new-224eef88-2b8f-4723-9db2-2370b7e0f2e2';
    $pane = new stdClass();
    $pane->pid = 'new-3cae31de-8cb7-4b34-b9f3-44a7258e0a29';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3cae31de-8cb7-4b34-b9f3-44a7258e0a29';
    $display->content['new-3cae31de-8cb7-4b34-b9f3-44a7258e0a29'] = $pane;
    $display->panels['content'][0] = 'new-3cae31de-8cb7-4b34-b9f3-44a7258e0a29';
    $pane = new stdClass();
    $pane->pid = 'new-3e0c4018-19e0-49c5-9f41-1f73119fc689';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'fi_core-fi_core_footer_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--three-tenths lap--one-third palm--one-half',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3e0c4018-19e0-49c5-9f41-1f73119fc689';
    $display->content['new-3e0c4018-19e0-49c5-9f41-1f73119fc689'] = $pane;
    $display->panels['footer'][0] = 'new-3e0c4018-19e0-49c5-9f41-1f73119fc689';
    $pane = new stdClass();
    $pane->pid = 'new-75c85c3e-a0df-47e2-854e-d0b8b96ea56e';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-helpful-links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--three-tenths lap--one-third palm--one-half',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '75c85c3e-a0df-47e2-854e-d0b8b96ea56e';
    $display->content['new-75c85c3e-a0df-47e2-854e-d0b8b96ea56e'] = $pane;
    $display->panels['footer'][1] = 'new-75c85c3e-a0df-47e2-854e-d0b8b96ea56e';
    $pane = new stdClass();
    $pane->pid = 'new-2b001cfd-c5ac-4a39-8d2e-d699c8b3691a';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'fi_core-fi_footer_affiliates';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third lap--one-third',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '2b001cfd-c5ac-4a39-8d2e-d699c8b3691a';
    $display->content['new-2b001cfd-c5ac-4a39-8d2e-d699c8b3691a'] = $pane;
    $display->panels['footer'][2] = 'new-2b001cfd-c5ac-4a39-8d2e-d699c8b3691a';
    $pane = new stdClass();
    $pane->pid = 'new-85b168ea-28a0-4a01-b500-301712ca4b9a';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_core-copyright';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '85b168ea-28a0-4a01-b500-301712ca4b9a';
    $display->content['new-85b168ea-28a0-4a01-b500-301712ca4b9a'] = $pane;
    $display->panels['footer_bottom'][0] = 'new-85b168ea-28a0-4a01-b500-301712ca4b9a';
    $pane = new stdClass();
    $pane->pid = 'new-5c4e9696-5831-4469-94bc-d88cf5e44f54';
    $pane->panel = 'header';
    $pane->type = 'pane_header';
    $pane->subtype = 'pane_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = '5c4e9696-5831-4469-94bc-d88cf5e44f54';
    $display->content['new-5c4e9696-5831-4469-94bc-d88cf5e44f54'] = $pane;
    $display->panels['header'][0] = 'new-5c4e9696-5831-4469-94bc-d88cf5e44f54';
    $pane = new stdClass();
    $pane->pid = 'new-7ccaf9bd-6c3d-4137-adf6-76b9f440e306';
    $pane->panel = 'header_top';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7ccaf9bd-6c3d-4137-adf6-76b9f440e306';
    $display->content['new-7ccaf9bd-6c3d-4137-adf6-76b9f440e306'] = $pane;
    $display->panels['header_top'][0] = 'new-7ccaf9bd-6c3d-4137-adf6-76b9f440e306';
    $pane = new stdClass();
    $pane->pid = 'new-d7b2f4ad-2243-4af6-a7e9-be0e7990a2e8';
    $pane->panel = 'navigation';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd7b2f4ad-2243-4af6-a7e9-be0e7990a2e8';
    $display->content['new-d7b2f4ad-2243-4af6-a7e9-be0e7990a2e8'] = $pane;
    $display->panels['navigation'][0] = 'new-d7b2f4ad-2243-4af6-a7e9-be0e7990a2e8';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['site_template_panel_fi_panels'] = $handler;

  return $export;
}
