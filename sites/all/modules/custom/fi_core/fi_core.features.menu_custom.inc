<?php
/**
 * @file
 * fi_core.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function fi_core_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-helpful-links.
  $menus['menu-helpful-links'] = array(
    'menu_name' => 'menu-helpful-links',
    'title' => 'Helpful Links',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Helpful Links');


  return $menus;
}
