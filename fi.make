; GK Distro
includes[gk_distro] = http://code.gkdigital.co/gk_distro/v1.0.6/gk_distro.make

; Site modules
projects[gk_bugherd][type] = module
projects[gk_bugherd][subdir] = standard
projects[gk_bugherd][download][type] = git
projects[gk_bugherd][download][url] = git@bitbucket.org:greeneking/gk-bugherd.git
projects[gk_bugherd][download][branch] = 7.x-1.0

projects[gk_locations][type] = module
projects[gk_locations][subdir] = standard
projects[gk_locations][download][type] = git
projects[gk_locations][download][url] = git@bitbucket.org:greeneking/gk-locations.git
projects[gk_locations][download][tag] = 7.x-1.11

projects[gk_promotions][type] = module
projects[gk_promotions][subdir] = standard
projects[gk_promotions][download][type] = git
projects[gk_promotions][download][url] = git@bitbucket.org:greeneking/gk-promotions.git
projects[gk_promotions][download][tag] = 7.x-1.1

projects[gk_social][type] = module
projects[gk_social][subdir] = standard
projects[gk_social][download][type] = git
projects[gk_social][download][url] = git@bitbucket.org:greeneking/gk-social.git
projects[gk_social][download][tag] = 7.x-1.1

projects[gk_webform][type] = module
projects[gk_webform][subdir] = standard
projects[gk_webform][download][type] = git
projects[gk_webform][download][url] = git@bitbucket.org:greeneking/gk-webform.git
projects[gk_webform][download][tag] = 7.x-1.2

; Site libraries
projects[headroom][type] = library
projects[headroom][subdir] = ""
projects[headroom][download][type] = git
projects[headroom][download][url] = https://github.com/WickyNilliams/headroom.js.git
projects[headroom][download][tag] = v0.4.0

projects[navinate][type] = library
projects[navinate][subdir] = ""
projects[navinate][download][type] = git
projects[navinate][download][url] = git@bitbucket.org:greeneking/navinate.git
projects[navinate][download][tag] = 0.1.0
